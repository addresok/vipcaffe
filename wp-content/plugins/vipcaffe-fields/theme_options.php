<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
    Container::make( 'theme_options', __( 'Настройка темы', 'theme' ) )
        ->add_tab( 'Заг. налаштування', array(
            Field::make('image', 'no_image', 'Картинка - заглушка')->help_text('Виводиться якщо в товара не задано основного зображення')

        ) )
        ->add_tab( 'Основное', array(
		        Field::make('text', 'title', 'Заголовок'),
		        Field::make('text', 'sub_title', 'Підзаголовок'),
		        Field::make("rich_text", "text", "текст")
        ) )
        ->add_tab( 'Контакты', array(

            Field::make( 'text', 'cont_number', 'Номер' )->set_width( 50 ),
            Field::make( 'text', 'cont_email', 'Email' )->set_width( 50 ),
            
            Field::make( 'text', 'url_fb', 'Фейсбук' )->set_width( 100 / 3 ),
            Field::make( 'text', 'url_tw', 'Твиттер' )->set_width( 100 / 3 ),
            Field::make( 'text', 'url_in', 'Инстаграм' )->set_width( 100 / 3 ),

        ) );
}