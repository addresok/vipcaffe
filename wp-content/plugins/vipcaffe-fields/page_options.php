<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;






add_action( 'carbon_fields_register_fields', 'crb_attach_portfolio_options' );
function crb_attach_portfolio_options() {
	Container::make( 'post_meta', __( 'Налаштування', 'product' ) )
			 ->show_on_post_type('product')
	         ->add_tab( 'Product Type', array(


                 Field::make("select", "product_type_type", "Тип")
                     ->set_options(array('melena' => 'Мелена'))
                     ->add_options(array(
                         'capsul'   => 'Капсульна',
                         'melena'   => 'Мелена',
                         'zernova'  => 'Зернова',
                     ))->set_width( 100 / 4 ),

                 Field::make("select", "product_type_type_tara", "Тип тари")
                     ->add_options(array(
                         'upakovka'     => 'В упаковці',
                         'jashik'       => 'В ящику',
                     ))->set_width( 100 / 4 ),


                 Field::make( 'text', 'product_type_value', 'Значення' )->set_width( 100 / 4 )
                        ->set_default_value(0),

                 Field::make("select", "product_type_type_odunits", "Одиниці")
                     ->add_options(array(
                         'capsul'     => 'Капсул',
                         'shtuk'       => 'Штук',
                         'gram'       => 'Грам',
                         'kilogram'       => 'Килограм',
                     ))->set_width( 100 / 4 ),


             ) )
		->add_tab( 'Галерея', array(
                Field::make( 'complex', 'gallery', 'Галерея' )
                    ->add_fields( array(

                            Field::make('image', 'image', 'image')
                        )
                    )
                    ->help_text( 'є можливість додати зображення пачкою.' )


            )
		);



    Container::make( 'post_meta', 'Інформація' )
        ->show_on_post_type('product')
        ->set_context('side')
        ->add_fields(array(

            Field::make("text", "price", "Вартість")
                ->set_default_value(0),
            Field::make("text", "price_sale", "Скидка")
                ->set_default_value(0),

        ));

}

/*
add_action( 'carbon_fields_register_fields', 'crb_attach_portfolio_texts' );
function crb_attach_portfolio_texts() {
	Container::make( 'term_meta', __( 'Тексти..', 'portfolio_text_header' ) )
	         ->show_on_taxonomy( 'portfolio' )
	         ->add_tab( 'Тексти для шапки', array(
		         Field::make('text', 'sub_title', 'Підзаголовок'),
		         Field::make("rich_text", "text", "текст"),


	         ) );

}*/