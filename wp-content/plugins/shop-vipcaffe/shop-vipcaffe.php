<?php
/*
Plugin Name: Shop VipCaffe
Description: Каталог
Version: 99.9
Author: xemlab Suplyk
Author URI: https://xemlab.com
*/



function initShopTaxonomy()
{
    register_taxonomy(
        'catalog',
        'product',
        array(
            'labels' => array(
                'name' => "Категорії",
                'add_new_item' => "Додати товар",
                'new_item_name' => "Додати товар",
                'new_item_name' => "Добавити категорію"
            ),
            'show_ui' => true,
            'show_tagcloud' => true,
            'hierarchical' => true,
            'rewrite'               => array( 'slug' => 'shop' ),
            'show_in_rest'          => true,
            'rest_base'             => 'shop',
            'rest_controller_class' => 'WP_REST_Terms_Controller',
        )
    );

    register_taxonomy(
        'product_status',
        'product',
        array(
            'labels' => array(
                'name' => "Статуси",
            ),
            'show_ui' => true,
            'show_tagcloud' => true,
            'hierarchical' => true,
            'rewrite'               => array( 'slug' => 'product_status' ),
            'show_in_rest'          => true,
            'rest_base'             => 'product_status',
            'rest_controller_class' => 'WP_REST_Terms_Controller',
        )
    );




    register_post_type('product',
        array(
            'labels' => array(
                'name' => "Товари",
                'singular_name' => "Товар",
                'add_new' => 'Добавити товар',
                'add_new_item' => "Добавити категорію",
                'edit' => "Редагувати",
                'edit_item' => 'Редагувати',
                'new_item' => 'Новий робота',
                'view' => 'Дивитися',
                'view_item' => 'Переглянути товар',
                'search_items' => 'Пошук',
                'not_found' => 'Обзор',
                'not_found_in_trash' => 'Немає записів',
                'parent' => 'Батьківський товар'
            ),
            'public' => true,
            'menu_position' => 7,
            'supports' => array('title', 'editor', 'thumbnail'),
            'taxonomies' => array('catalog', 'product_cat'),
            'menu_icon' => 'dashicons-cart',
            'capability_type'    => 'post',

            'has_archive' => true,
            'rewrite'            => array( 'slug' => 'product' ),
            'show_in_rest'       => true,
            'rest_base'          => 'product',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
        )
    );
    flush_rewrite_rules();
}

add_action('init', 'initShopTaxonomy');



function prepare_rest($data, $post, $request){
    $_data = $data->data;

    $thumbnail_id = get_post_thumbnail_id($post->ID);


    if($thumbnail_id) {
        $thumbnail_medium = wp_get_attachment_image_src($thumbnail_id, 'medium');
        $thumbnail_large = wp_get_attachment_image_src($thumbnail_id, 'large');
        $thumbnail_thumbnail = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');
    } else {
        $thumbnail_id_no_image = carbon_get_theme_option('no_image');
        $thumbnail_medium       = wp_get_attachment_image_src($thumbnail_id_no_image, 'medium');
        $thumbnail_large        = wp_get_attachment_image_src($thumbnail_id_no_image, 'large');
        $thumbnail_thumbnail    = wp_get_attachment_image_src($thumbnail_id_no_image, 'thumbnail');
    }



    $_data['images']['medium'] = $thumbnail_medium[0];
    $_data['images']['large'] = $thumbnail_large[0];
    $_data['images']['thumbnail'] = $thumbnail_thumbnail[0];

    $filds = get_post_custom($post->ID);
    $_data['excerpt'] = get_the_excerpt();

    $price = ( isset($filds['_price'][0]) && trim($filds['_price'][0])) ? $filds['_price'][0] : '0';
    $price_sale = ( isset($filds['_price_sale'][0]) && trim($filds['_price_sale'][0])) ? $filds['_price_sale'][0] : '0';
    $_data['price'] = $price;
    $_data['price_sale'] = $price_sale;
    $_data['is_cart'] = 0;

    $_data['product_type'] = [];


    $_data['product_type']['type'] = carbon_get_post_meta( $post->ID, 'product_type_type');
    $_data['product_type']['tara'] = carbon_get_post_meta( $post->ID, 'product_type_type_tara');
    $_data['product_type']['value'] = carbon_get_post_meta( $post->ID, 'product_type_value');
    $_data['product_type']['odunits'] = carbon_get_post_meta( $post->ID, 'product_type_type_odunits');




    $_data['gallery'] = [];

    if($thumbnail_id)
    $_data['gallery'][] = [
        'thumb' => $_data['images']['thumbnail'],
        'medium' => $_data['images']['medium'],
        'large' => $_data['images']['large'],
    ];


    $gallery = (array) carbon_get_post_meta( $post->ID, 'gallery');;



    foreach ( $gallery as $val){




        $_data['gallery'][] = [
            'thumb' => wp_get_attachment_image_src($val['image'], 'thumbnail')[0],
            'medium' => wp_get_attachment_image_src($val['image'], 'medium')[0],
            'large' => wp_get_attachment_image_src($val['image'], 'large')[0],
        ];


    }





    $data->data = $_data;

    return $data;
}
add_filter('rest_prepare_product', 'prepare_rest',10, 3);




